<div id='mobi-nav'>
	<div class='mobi-nav-inner'>
		<div class='topper'>
			<span class='mobile-nav-link fa fa-times'></span>
			<div class="sidebar-logo">
				<img src="<?php bloginfo('template_directory'); ?>/lib/img/larg-logo.png">
			</div>
		</div>
		<div class="mn-body">
			<ul>
				<li class="even"><a href="<?php bloginfo('url'); ?>">Home</a></li>
				<li class="odd"><a href="<?php bloginfo('url'); ?>/about-us">About Us</a></li>
				<li class="even"><a href="<?php bloginfo('url'); ?>/the-team">The Team</a></li>
				<li class="odd"><a href="<?php bloginfo('url'); ?>/constructon-projects">Construction Projects</a></li>
				<li class="even"><a href="<?php bloginfo('url'); ?>/commercial-cleaning">Commercial Cleaning</a></li>
				<li class="odd"><a href="<?php bloginfo('url'); ?>/request-a-quote">Request A Quote</a></li>
				<li class="even"><a href="<?php bloginfo('url'); ?>/employment-opportunities">Employment Opportunities</a></li>
				<li class="odd"><a href="<?php bloginfo('url'); ?>/contact-us">Contact Us</a></li>
			</ul>
		</div>
	</div>
</div>