<ul class="services-sidebar--list">

	<?php 
		if ( is_page( 'commercial-cleaning' ) ) {
		    $args = array( 'service-level' => 'commercial-cleaning', 'post_type' => 'services', 'posts_per_page' => -1, 'order' => 'ASC' );
		} else {
		    $args = array( 'service-level' => 'construction-projects', 'post_type' => 'services', 'posts_per_page' => -1, 'order' => 'ASC' );
		}

		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post(); 
	?>

	<li class="service">
	  <a href="#<?php the_title(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
	</li>

	<?php endwhile; wp_reset_postdata(); ?>

</ul>