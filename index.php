<?php get_header(); ?>
			<section id='hero'></section>
			<section id='cta-bar'>
				<div class="wrapper">
					<span>Get a <span class="cta-highlight">Free Estimate</span> on your project today!</span>
					<a href="#" title="Get A Free Quote" class="cta-btn btn orange radius">Get it started</a>
				</div>
			</section>
			<section id='body-content'></section>
<?php get_footer(); ?>