		<footer>
			<div id="contact-us" class="main">
				<div class="wrapper">
					<div class="footer-head">
						<h3>Contact Us</h3>
						<p>Do you have a project you would like us to work on? Or perhaps a few questions?</p>
						<p>Contact us and we will be more than happy to assist you.</p>
					</div>
					<div class="footer-content">
						<div class="contact info">
							<h4>Our office</h4>
							<ul>
								<li><span class="fa fa-phone"></span><a href="tel:7205056223">(720) 505-6223</a></li>
								<li>303 S. Broadway Ste. 200-708<br>Denver, CO 80209</li>
							</ul>
						</div>
						<div class="contact form">
							<h4>Say hello</h4>
							<!--form>
								<input type="text" id="footer-name" placeholder="Name" class="float">
								<input type="text" id="footer-telephone" placeholder="Telephone" class="float">
								<input type="text" id="footer-email" placeholder="email" class="float">
								<div class="floatlabel-wrapper">
									<textarea name="footer-comment" placeholder="comment"></textarea>
								</div>
								<input type="submit" id="submit" value="Send">
							</form-->
							<?php echo do_shortcode( '[contact-form-7 id="4" title="Footer Contact Form"]' ); ?>
						</div>
						<div class="contact social">
							<h4>Keep connected</h4>
							<ul id="footer-social">
								<li class="facebook"><a href="#" title="Facebook"><span class="fa fa-facebook-official"></span></a></li>
								<li class="yelp"><a href="#" title="Yelp"><span class="fa fa-yelp"></span></a></li>
								<li class="youtube"><a href="#" title="YouTube"><span class="fa fa-youtube-square"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="copyright">
				<div class="wrapper">
					<div class='footer-copyright'>
						<span>&copy;<?php echo date("Y") ?> <?php bloginfo('name'); ?> | All Rights Reserved</span>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/lib/js/floatlabels.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/lib/js/init.js" type="text/javascript"></script>

<?php wp_footer(); ?>
</body>
</html>