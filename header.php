<!DOCTYPE html>
<html>
	<head>
		<title><?php bloginfo('name'); ?><?php wp_title('|', true, 'left'); ?></title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,400italic|Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/lib/css/style.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php require("templates/favicons.php"); ?>

		<?php wp_head(); ?>
		
	</head>
	<body <?php body_class(); ?> >
		<div id='site-wrapper'>
			<?php get_template_part('templates/mobile','nav'); ?>
			<div id='page-wrap'>
				<header>
					<div class='top'>
						<div class='wrapper'>
							<div id='logo'>
								<a href='<?php bloginfo('url'); ?>' title='<?php bloginfo('name'); ?>'>
									<h1>
									<img alt='Denver Pro Painting &amp; Construction Inc.' src='<?php bloginfo('template_directory'); ?>/lib/img/denver-pro_logo-round.png'>
									</h1>
								</a>
							</div>
							<?php get_template_part('templates/main','nav'); ?>
							<span class='mobile-nav-link fa fa-bars'></span>
						</div>
					</div>
					<div class='sub-bar'>
						<div class='wrapper'>
							<span>&nbsp;</span>
							<ul class='sub-nav'>
								<li><a href="<?php bloginfo('url'); ?>">Home</a></li>
								<li><a href="<?php bloginfo('url'); ?>/about-us">About Us</a></li>
								<li><a href="<?php bloginfo('url'); ?>/the-team">The Team</a></li>
								<li class="quote-request"><a href="<?php bloginfo('url'); ?>/request-a-quote">Request A Quote</a></li>
								<li><a href="<?php bloginfo('url'); ?>/contact-us">Contact Us</a></li>
							</ul>
							<span>&nbsp;</span>
						</div>
					</div>
				</header>