<?php 
	if ( is_page( 'commercial-cleaning' ) ) {
	    $args = array( 'service-level' => 'commercial-cleaning', 'post_type' => 'services', 'posts_per_page' => -1, 'order' => 'ASC' );
	} else {
	    $args = array( 'service-level' => 'construction-projects', 'post_type' => 'services', 'posts_per_page' => -1, 'order' => 'ASC' );
	}

	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); 
?>

	<div id="<?php the_title(); ?>" class="service">
		<h3><?php the_title(); ?></h3>
		<p><?php the_content(); ?></p>
	</div>


<?php endwhile; wp_reset_postdata();?>