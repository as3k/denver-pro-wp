<div class="team-member">
	<div class="team-member-photo">
		<img src="<?php the_field('staff_member_photo'); ?>" alt="<?php the_title(); ?>"/>
	</div>
	<h2><?php the_title(); ?>
	<span class="team-title"><?php the_field( 'position' ); ?></span></h2>
	<?php the_content(); ?>

	<?php if( get_field( 'points_of_interest' ) ) { ?>
		<?php
		$title = get_the_title(); // This must be!, because this is the return - the_title would be echo
		$title_array = explode(' ', $title);
		$first_word = $title_array[0];

		echo $first_word;
		?>'s&nbsp;

		<?php the_field( 'poi_title' ); ?>

		<?php 
			$poi = get_field( 'poi_list' ); 
			$list = explode( ", ", $poi );
			if( !empty( $list ) ) {
			    echo '<ul class="vertical-list">';
			    foreach( $list as $item ){
			        echo '<li>'.htmlspecialchars( $item ).'</li>';
			    }
			    echo '</ul>';
			}
		?>

	<?php }  ?>
</div><!-- end team-member -->