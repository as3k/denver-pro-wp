<?php if ( is_active_sidebar( 'right_1' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'right_1' ); ?>
	</div><!-- #primary-sidebar -->
<?php endif; ?>