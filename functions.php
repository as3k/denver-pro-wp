<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Right sidebar',
		'id'            => 'right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );

// Create Staff Members Taxonomy
add_action( 'init', 'create_board_member_tax', 0 );
function create_board_member_tax() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Staff Types', 'taxonomy general name' ),
		'singular_name'     => _x( 'Staff Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Staff Type' ),
		'all_items'         => __( 'All Staff Types' ),
		'parent_item'       => __( 'Parent Staff Type' ),
		'parent_item_colon' => __( 'Parent Staff Type:' ),
		'edit_item'         => __( 'Edit Staff Type' ),
		'update_item'       => __( 'Update Staff Type' ),
		'add_new_item'      => __( 'Add New Staff Type' ),
		'new_item_name'     => __( 'New Staff Type' ),
		'menu_name'         => __( 'Staff Types' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'staff-members' ),
	);

	register_taxonomy( 'staff-level', array( 'staff-members' ), $args );
}

// Create Services Taxonomy
add_action( 'init', 'create_services_tax', 0 );
function create_services_tax() {
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _( 'Service Types', 'taxonomy general name' ),
		'singular_name'     => _( 'Service Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Service Type' ),
		'all_items'         => __( 'All Service Types' ),
		'parent_item'       => __( 'Parent Service Type' ),
		'parent_item_colon' => __( 'Parent Service Type:' ),
		'edit_item'         => __( 'Edit Service Type' ),
		'update_item'       => __( 'Update Service Type' ),
		'add_new_item'      => __( 'Add New Service Type' ),
		'new_item_name'     => __( 'New Service Type' ),
		'menu_name'         => __( 'Service Types' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
	);

	register_taxonomy( 'service-level', array( 'services' ), $args );
}

// custom post types
add_action( 'init', 'create_post_type' );
function create_post_type() {

  	// custom post type => Staff Members
  	register_post_type( 'staff-members',
	    array(
	      	'labels' => array(
				'name' => __( 'Staff Members' ),
				'all_items' => __( 'All Staff Members' ),
	    	    'singular_name' => __( 'Staff Member' ),
	    	    'add_new' => __( 'Add New Staff Member' ),
	    	    'add_new_item' => __( 'Add New Staff Member' ),
	    	    'edit' => __( 'Edit Staff Member' ),
	    	    'edit_item' => __( 'Edit Staff Member' ),
	    	    'new_item' => __( 'New Staff Member' ),
	    	    'view' => __( 'View Staff Member' ),
	    	    'view_item' => __( 'View Staff Member' ),
	    	    'search_items' => __( 'Search Staff Members' ),
	    	    'not_found' => __( 'No Staff Members found' ),
	    	    'not_found_in_trash' => __( 'No Staff Members found in Trash' ),
	      	),
	      	'public' => true,
	      	'has_archive' => true,
	      	'taxonomies' => array( 'member-level' ),
	      	'menu_position' => 6,
	      	'menu_icon' => 'dashicons-groups',
	      	'rewrite' => array( 'slug' => 'the-team' ),
	    )
  	);
  	// End Staff Post Type

  	// custom post type => services
  	register_post_type( 'services',
	    array(
	      	'labels' => array(
				'name' => __( 'Services' ),
				'all_items' => __( 'All Services' ),
	    	    'singular_name' => __( 'Service' ),
	    	    'add_new' => __( 'Add New Service' ),
	    	    'add_new_item' => __( 'Add New Service' ),
	    	    'edit' => __( 'Edit Service' ),
	    	    'edit_item' => __( 'Edit Service' ),
	    	    'new_item' => __( 'New Service' ),
	    	    'view' => __( 'View Service' ),
	    	    'view_item' => __( 'View Service' ),
	    	    'search_items' => __( 'Search Servicess' ),
	    	    'not_found' => __( 'No Servicess found' ),
	    	    'not_found_in_trash' => __( 'No services found in Trash' ),
	      	),
	      	'public' => true,
	      	'has_archive' => true,
	      	'taxonomies' => array( 'service-level' ),
	      	'menu_position' => 6,
	      	'menu_icon' => 'dashicons-clipboard',
	      	//'rewrite' => array( 'slug' => 'the-team' ),
	    )
  	);
  	// End Staff Post Type
}

?>