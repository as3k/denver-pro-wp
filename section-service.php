<article id="<?php $slug = get_post_field( 'post_name', get_post() ); echo $slug;?>">
    <div class="service-header">
      <h3>
        <?php the_title(); ?>
      </h3>
    </div>
    <div class="service-content">
      <?php the_content(); ?>  
    </div>
</article>