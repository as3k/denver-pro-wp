		<div class="main team-wrap">
			<?php 
						$main = array (
							'numberposts' 	=> -1,
							'post_type' 	=> 'staff-members',
							// 'meta_key' 		=> 'staff_member_location',
							// 'meta_value' 	=> 'Main Office',
							'order' 		=> 'ASC'
						);

						$mainLoop = new WP_Query( $main );
						if( $mainLoop->have_posts() ): {
							while( $mainLoop->have_posts() ) : $mainLoop->the_post(); {
								get_template_part( 'section','team-member' );
							} endwhile;
						} endif;
						wp_reset_postdata(); // reset to the original page data
			?>
		</div>