<?php 
$post = $wp_query->post;
get_header(); 
?>

<div class="body-content">
	<div class="wrapper">
		<?php
		// If the page is the staff page, get 
		// the staff members page template part if not
		// then get the regular page template for 
		// displaying content
		if( is_page( '6' ) ) { ?>
			<!-- <div class="team-wrap"> -->
				<?php get_template_part('section','team'); ?>
			<!-- </div> -->
		<?php } else {
			get_template_part('section','main-content');
		} ?>

		<div class="sidebar">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>