var gulp = require("gulp"),
	uglify = require("gulp-uglify"),
	less = require("gulp-less"),
	livereload = require("gulp-livereload")

// Script Task
// Uglifies
gulp.task('scripts', function(){
	gulp.src('lib/js/*.js')
	.pipe(uglify())
	.pipe(gulp.dest('lib/js/build.js'));
});

// Less Compile Task
// Compile any file in the less DIR and 
// it's child DIRs that are saved.
gulp.task('less', function(){
	gulp.src('lib/less/style.less')
	.pipe( less() )
	.pipe( gulp.dest( 'lib/css' ) )
	.pipe( livereload() );
});

// Watch Task
// Watched Less DIR for changes and writes them
gulp.task('watch', function(){
	// var server = livereload();
	livereload.listen();
	gulp.watch( 'lib/less/**/*.less', ['less'] );
});