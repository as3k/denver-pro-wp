<?php 
/* Template Name: Service List */
$post = $wp_query->post;
get_header(); ?>

<div class="body-content <?php $slug = get_post_field( 'post_name', get_post() ); echo $slug;?>">
	<div class="wrapper">
		<div class="main">

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
        <section class="services service-content">
          <h2 class="page-title"><?php the_title(); ?></h2>
          <?php the_content(); ?>
        </section><!-- end services service-content -->
        
      <?php endwhile; else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
      <?php endif; ?>

      
      <section class="services service-list clearfix">
        <div class="services-sidebar">
          <?php get_template_part( 'section', 'service-sidebar' ); ?>
        </div>
        <div class="services-list">
				  <?php get_template_part( 'section','service-loop' ); ?>
        </div>
			</section>
      
		</div>
	</div>
</div>

<?php get_footer(); ?>

