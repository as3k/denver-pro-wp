<?php 
/* Template Name: Denver Pro Homepage */
get_header(); ?>
			<section id='hero'>
				<?php 
    echo do_shortcode("[metaslider id=109]"); 
?>
			</section>
			<section id='cta-bar'>
				<div class="wrapper">
					<span>Get a <span class="cta-highlight">Free Estimate</span> on your project today!</span>
					<a href="#" title="Get A Free Quote" class="cta-btn btn orange radius">Get it started</a>
				</div>
			</section>
			<section id='body-content'>
				<div class="wrapper">
				
					<?php 		

						if (have_posts()) : while (have_posts()) : the_post(); 
						        
							the_content(); 
						          
						endwhile; endif; 	     

					?>
			
				</div>
			</section>
<?php get_footer(); ?>